#!/usr/bin/env python3
import warnings
import logging
import openpyxl
from openpyxl.utils import get_column_letter, column_index_from_string
from openpyxl.styles import NamedStyle, Border, Side, Alignment, PatternFill
from openpyxl.formatting import Rule
from openpyxl.styles.differential import DifferentialStyle

from lex import *

CODE_ROW = 6 # Row of board code
CODE_COL = 3 # Column of the first board code
CODE_OFFSET = 4 # Offset of a board w.r.t. the previous one
MOVES_ROW = 8 # Row of the first move
MOVES_OFFSET = -2 # Offset of moves w.r.t. CODE_COL

STYLE = NamedStyle(name="default")
BORDER = Side(style='thin', color="000000")
STYLE.border = Border(left=BORDER, top=BORDER, right=BORDER, bottom=BORDER)
STYLE.alignment = Alignment(horizontal="center", vertical="center")
CYAN_STYLE = Rule(type='cellIs', operator='equal',
                  dxf=DifferentialStyle(fill=PatternFill(bgColor='00FFFF')),
                  formula=["{}".format(CYAN)])
VIOLET_STYLE = Rule(type='cellIs', operator='equal',
                    dxf=DifferentialStyle(fill=PatternFill(bgColor='EE82EE')),
                    formula=["{}".format(VIOLET)])

def find_board_column(filename, turn, code):
    """Return the column of the board identified by code and turn.

    None if code is not found.
    """
    warnings.simplefilter("ignore")
    wb = openpyxl.load_workbook(filename, read_only=True, data_only=True)
    ws = wb['Turn{}'.format(turn)]
    ws.calculate_dimension(force=True) # Google sheets save as unsized
    for col in range(CODE_COL, ws.max_column+1, CODE_OFFSET):
        c = ws.cell(row=CODE_ROW, column=col)
        if c.value is not None:
            if c.value == code:
                wb.close()
                return c.column
    wb.close()
    return None


def get_moves(filename, code, turn, player=VIOLET):
    """Get the moves associated to a given board and turn.

    Return a pair of lists: the possible moves and the current weights,
    None if the board is not present in filename.
    Illegal moves are always implicitly weighted 0, thus they are never returned, rewarded or punished.
    """
    col = find_board_column(filename, turn, code)
    if col is not None:
        moves_col = col + MOVES_OFFSET
        moves, weights = [], []
        warnings.simplefilter("ignore")
        wb = openpyxl.load_workbook(filename, read_only=True, data_only=True)
        ws = wb['Turn{}'.format(turn)]
        for r in range(MOVES_ROW, MOVES_ROW+len(WELL_FORMED_MOVES)+1):
            move_cell = ws[get_column_letter(moves_col)+str(r)]
            weight_cell = ws[get_column_letter(moves_col+1)+str(r)]
            if move_cell.value is not None:
                legal_moves = get_legal_moves(from_code(code), player)
                if move_cell.value in legal_moves:
                    moves.append(move_cell.value)
                    assert weight_cell.value is not None, "None weight {} {}".format(code, turn)
                    weights.append(weight_cell.value)
                else:
                    moves.append(move_cell.value)
                    weights.append(0)

        wb.close()
        return moves, weights
    return None

def reward(filename, newfile, match_moves, prize):
    """Write a new Excel file with every move in match_moves rewarded.
    """

    warnings.simplefilter('ignore')
    wb = openpyxl.load_workbook(filename, data_only=True)
    for move, code, turn in match_moves:
        ws = wb['Turn{}'.format(turn)]
        col = find_board_column(filename, turn, code)
        if col is not None:
            moves_col = col + MOVES_OFFSET
            for r in range(MOVES_ROW, MOVES_ROW+len(WELL_FORMED_MOVES)+1):
                move_cell = ws[get_column_letter(moves_col)+str(r)]
                weight_cell = ws[get_column_letter(moves_col+1)+str(r)]
                if move_cell.value == move:
                    weight_cell.value = weight_cell.value + prize
                    break
    wb.save(newfile)
    wb.close()


def choose_and_remember_move(filename, code, turn, match_moves, rand, player=VIOLET):
    """Randomly choose a move, append info in match_moves to reward in case of a win.

    """

    def choose_move(h):
        moves_weights = get_moves(filename, h, turn, player)
        if moves_weights is not None:
            moves, weights = moves_weights
            if sum(weights) > 0:
                m = rand.choices(moves, weights)
                logging.debug('Choices: {} {} {}'.format(moves, weights, m))
            else:
                m = moves[0:1]
            return m[0]
        else:
            return None

    move = choose_move(code)
    if move is not None:
        match_moves.append((move, code, turn))
        return move

    move = choose_move(code[::-1])
    if move is not None:
        match_moves.append((move, code[::-1], turn))
        return flip_move(move)

def make_empty_experience(filename='exapawn-empty.xls', players=[VIOLET, CYAN]):
    _, boards = get_forest()
    wb = openpyxl.Workbook()
    for player in players:
        maxturn = 0
        for t, _ in boards.values():
            if t > maxturn:
                maxturn = t
        for turn in range(player, maxturn+1, 2):
            name = 'Turn{}'.format(turn)
            ws = wb.create_sheet(name)
            code_col = CODE_COL
            for code in boards:
                t, moves = boards[code]
                if t == turn:
                    board = from_code(code)
                    for i, c in enumerate(['X', 'Y', 'Z']):
                        col = code_col - 3 + i + 1
                        x = ws.cell(row=1, column=col, value=c)
                        x.style = STYLE
                        for r in range(2, 2+len(board[c])):
                            x = ws.cell(row=r, column=col, value=board[c][r-2])
                            x.style = STYLE
                    x = ws.cell(row=CODE_ROW, column=code_col, value=code)
                    x.style = STYLE

                    moves_row=MOVES_ROW
                    for m in sorted(moves): # sort values for reproducibility
                        x = ws.cell(row=moves_row, column=code_col+MOVES_OFFSET, value=m)
                        x.style = STYLE
                        x = ws.cell(row=moves_row, column=code_col+MOVES_OFFSET+1, value=1)
                        x.style = STYLE
                        moves_row = moves_row + 1

                    code_col = code_col + CODE_OFFSET
            ws.conditional_formatting.add('A2:{}4'.format(get_column_letter(code_col)), CYAN_STYLE)
            ws.conditional_formatting.add('A2:{}4'.format(get_column_letter(code_col)), VIOLET_STYLE)
    del wb['Sheet']
    wb.save(filename)


import unittest, os, random

class TestLexcel(unittest.TestCase):

    def setUp(self):
        self.EMPTY = 'tests/exapawn-empty.xlsx'
        self.TMP = 'tests/test-exapawn-tmp.xlsx'
        self.RAND = random.Random(42)

    def tearDown(self):
        if os.path.isfile(self.TMP):
            os.remove(self.TMP)

    def test_find_board(self):
        cell = find_board_column(self.EMPTY, 2, '5BB')
        self.assertEqual(cell, column_index_from_string('C'))

        cell = find_board_column(self.EMPTY, 4, '38B')
        self.assertEqual(cell, column_index_from_string('G'))

        cell = find_board_column(self.EMPTY, 6, '830')
        self.assertEqual(cell, column_index_from_string('AA'))

    def test_get_moves(self):
        moves, weights = get_moves(self.EMPTY, '38B', 4, VIOLET)
        self.assertEqual(len(moves), 6)
        self.assertIn('YY', moves)
        self.assertIn('YX', moves)
        self.assertIn('YZ', moves)
        self.assertIn('ZZ', moves)
        self.assertIn('XX', moves) # illegal, weight 0
        self.assertIn('XY', moves) # illegal, weight 0
        self.assertEqual(sum(weights), 4)

    def test_choose_and_remember_move(self):
        match = []
        move = choose_and_remember_move(self.EMPTY, '895', 4, match, self.RAND, VIOLET)
        self.assertTrue(move == 'XX' or move == 'XY')
        self.assertEqual(match, [(move, '895', 4)])

    def test_choose_and_remember_move_reproducibile(self):
        match = []
        myrand = random.Random(666)
        old = []
        for j in range(10):
            old.append(choose_and_remember_move(self.EMPTY, '83B', 4, match, myrand, VIOLET))

        for i in range(2):
            myrand = random.Random(666)
            for j in range(10):
                move = choose_and_remember_move(self.EMPTY, '83B', 4, match, myrand, VIOLET)
                self.assertEqual(move, old[j])

    def test_reward(self):
        match = []
        choose_and_remember_move(self.EMPTY, '895', 4, match, self.RAND, VIOLET)
        reward(self.EMPTY, self.TMP, match, 1)
        self.assertTrue(os.path.isfile(self.TMP))
        moves, weights = get_moves(self.TMP, '895', 4, VIOLET)
        self.assertIn(2, weights)

    def test_punish(self):
        match = []
        choose_and_remember_move(self.EMPTY, '895', 4, match, self.RAND, VIOLET)
        reward(self.EMPTY, self.TMP, match, -1)
        self.assertTrue(os.path.isfile(self.TMP))
        moves, weights = get_moves(self.TMP, '895', 4, VIOLET)
        self.assertIn(0, weights)


    def test_get_moves_flipped(self):
        r = get_moves(self.EMPTY, 'B83', 4, VIOLET)
        self.assertIs(r, None)


    def test_choose_and_remember_move_flipped(self):
        match = []
        move = choose_and_remember_move(self.EMPTY, '335', 6, match, self.RAND, VIOLET)
        self.assertEqual(move, 'ZY')
        self.assertEqual(match, [(flip_move(move), '533', 6)])
        col = find_board_column(self.EMPTY, 6, match[0][1])
        self.assertTrue(col is not None)


    def test_reward_flipped(self):
        match = []
        choose_and_remember_move(self.EMPTY, '55F', 4, match, self.RAND, VIOLET)
        reward(self.EMPTY, self.TMP, match, 1)
        self.assertTrue(os.path.isfile(self.TMP))
        moves, weights = get_moves(self.TMP, 'F55', 4, VIOLET)
        self.assertIn(2, weights)

    def test_make_empty_experience(self):
        make_empty_experience(self.TMP, [VIOLET, CYAN])
        self.assertTrue(os.path.isfile(self.TMP))
        moves, weights = get_moves(self.TMP, 'B38', 4, VIOLET)
        self.assertEqual(len(moves), 4)
        self.assertEqual(sum(weights), 4)


if __name__ == '__main__':
    unittest.main()
