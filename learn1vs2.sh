#!/bin/bash

# Learn the best strategy with matches between automatic players
NUM_MATCHES=25

mkdir -p learn

./lex_game.py --empty learn/exapawn-empty.xlsx
echo "* -e1 learn/exapawn-empty.xlsx learn/cyan-001.xlsx -e2 learn/exapawn-empty.xlsx learn/violet-001.xlsx" > learn/match.log
./lex_game.py -e1 learn/exapawn-empty.xlsx learn/cyan-001.xlsx -e2 learn/exapawn-empty.xlsx learn/violet-001.xlsx | tee --append learn/match.log
for i in $(seq 1 $((NUM_MATCHES - 1))); do
    s=$(printf "%0.3d" "$i")
    e=$(printf "%0.3d" $(( i + 1 )))
    echo "* -e1 learn/cyan-$s.xlsx learn/cyan-$e.xlsx -e2 learn/violet-$s.xlsx learn/violet-$e.xlsx" >> learn/match.log
    ./lex_game.py -e1 "learn/cyan-$s.xlsx" "learn/cyan-$e.xlsx" -e2 "learn/violet-$s.xlsx" "learn/violet-$e.xlsx" | tee --append learn/match.log
done

# For a histogram of each player's points:
# grep won learn/match.log | awk -- '/1/ {a +=1; print a,b} /2/ {b += 1; print a,b}' | nl
