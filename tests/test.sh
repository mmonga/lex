CMD=python

if pip list | grep coverage; then
    CMD="$CMD -m coverage run"
fi

fail(){
    echo "$* failed"
    exit
}


$CMD lex.py || fail lex
$CMD -a lexcel.py || fail lexcel
$CMD -a lex_game.py < tests/winner1.txt | grep "Player 1 won" || fail winner1
$CMD -a lex_game.py < tests/winner2.txt | grep "Player 2 won" || fail winner2

$CMD -a lex_game.py -e2 tests/exapawn-empty.xlsx exapawn-7777.xlsx -b b89 -p 1 -t 3 -s 7777 < tests/7777.txt | grep "Player 2 won" || fail 7777
rm exapawn-7777.xlsx
$CMD -a lex_game.py --empty exapawn-empty.xlsx || fail empty
$CMD -a lex_game.py -e2 exapawn-empty.xlsx exapawn-test-001.xlsx -s 0001 -d test0001.log < tests/0001.txt | grep "Player 2 won" || fail 0001
$CMD -a lex_game.py -e2 exapawn-test-001.xlsx exapawn-test-002.xlsx -s 0102 < tests/0002.txt | grep "Player 2 won" || fail 0002
rm exapawn-test-001.xlsx exapawn-test-002.xlsx
$CMD -a lex_game.py -e2 exapawn-empty.xlsx exapawn-test-001c.xlsx -s 0002 < tests/0002c.txt | grep "Player 1 won" || fail 0002c
rm exapawn-empty.xlsx exapawn-test-001c.xlsx

$CMD -a lex_game.py --tree | grep -c '\[.*winner.*\]' | grep 65 || fail tree
echo "Success!"
