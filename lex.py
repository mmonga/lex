#!/usr/bin/env python3

EMPTY = 0
CYAN = 1
VIOLET = 2

INITIAL_BOARD = {'X': (VIOLET, EMPTY, CYAN),
                 'Y': (VIOLET, EMPTY, CYAN),
                 'Z': (VIOLET, EMPTY, CYAN),
}

WELL_FORMED_MOVES = ('XX', 'YY', 'ZZ', 'XY', 'YX', 'YZ', 'ZY')

MARKS = (' ', '♙', '♟')

def to_string(board, m=MARKS):
    """Return a string representation for the board.

    It uses Unicode code points for 'WHITE CHESS PAWN' and 'BLACK CHESS PAWN'.
    """
    s = """
   X   Y   Z
 +-----------+
"""
    for r in range(len(board['X'])):
        s += " | {} | {} | {} |\n".format(m[board['X'][r]],
                                          m[board['Y'][r]],
                                          m[board['Z'][r]])
        s += ' +-----------+\n'
    s += 'Board code: {}\n'.format(to_code(board))
    return s


def to_code(board):
    """Return a coded representation of a board for ease board matching.

    The code is a string of 3 alphanumeric digits representing columns,
    therefore a flipped board has exactly the reverse code.

    """
    r = ''
    DIGITS="0123456789ABCDEFGHIJKLMNOPQ"
    base = VIOLET+1
    assert len(DIGITS) == base**3, len(DIGITS)
    for c in sorted(board.keys()):
        n = 0
        for i, p in enumerate(board[c]):
            n += p * base**i
        r += DIGITS[n]
    return r.upper()



def from_code(code):
    """Return a board corresponding to code digits.
    """
    r = {}
    base = VIOLET + 1
    for i, c in enumerate(['X', 'Y', 'Z']):
        col = []
        n = int(code[i], base=base**3)
        for j in range(3):
            col.append(n % base)
            n = n // base
        r[c] = tuple(col)

    return r

def flip(board):
    """Return a board with the columns flipped.
    """
    r = {}
    r['X'] = board['Z']
    r['Y'] = board['Y']
    r['Z'] = board['X']
    return r

def flip_move(move):
    """Return a flipped move, with X and Z exchanged.

    """
    r = ''
    for c in move:
        if c == 'X':
            r = r + 'Z'
        elif c == 'Z':
            r = r + 'X'
        else:
            r = r + c
    return r

def exchange(board):
    """Return a board with players exchanged as if VIOLET played as CYAN and viceversa.
    """
    r = {}
    for c in board.keys():
        r[c] = [EMPTY, EMPTY, EMPTY]
        for i, p in enumerate(board[c][::-1]):
            r[c][i] = other(p)
        r[c] = tuple(r[c])
    return r

def can_move_fwd(board, player, column):
    """True if player can move forward in column.
    """
    if player == VIOLET:
        for row in [0, 1]:
            if board[column][row] == VIOLET and board[column][row+1] == EMPTY:
                return True
        return False
    else:
        return can_move_fwd(exchange(board), VIOLET, column)

def can_capture(board, player, column):
    """True if player can move diagonally from column and capture a pawn of the other color.
    """
    if player == VIOLET:
        if column == 'X':
            for row in [0, 1]:
                if board['X'][row] == VIOLET and board['Y'][row + 1] == CYAN:
                    return True
        elif column == 'Z':
            return can_capture(flip(board), VIOLET, 'X')
        elif column == 'Y':
            for row in [0, 1]:
                if board['Y'][row] == VIOLET and (board['X'][row + 1] == CYAN
                                                or board['Z'][row + 1] == CYAN):
                    return True
        return False
    else:
        return can_capture(exchange(board), VIOLET, column)

def other(player):
    """Return opposite color. """
    if player == VIOLET:
        return CYAN
    if player == CYAN:
        return VIOLET
    return EMPTY

def is_winner(board, player):
    """True if player is winning on board. """
    if player == VIOLET:
        opponent_moves = 0
        for c in ['X', 'Y', 'Z']:
            if board[c][-1] == VIOLET:
                return True
            if can_move_fwd(board, CYAN, c) or can_capture(board, CYAN, c):
                opponent_moves += 1
        return opponent_moves == 0
    return is_winner(exchange(board), VIOLET)


def move(board, player, start, end):
    """Return a board in which player has moved from columns start to column end.

    Raise an exception if the move is not legal.
    """
    assert start+end in WELL_FORMED_MOVES
    if player == VIOLET:
        if start == end and not can_move_fwd(board, VIOLET, start):
            raise Exception("{} cannot move forward on column {}".format(VIOLET, start))
        r = board.copy()
        if start == end:
            assert can_move_fwd(board, VIOLET, start)
            r[start] = list(board[start])
            i = len(r[start]) - 1 - r[start][::-1].index(VIOLET)
            r[start][i] = EMPTY
            r[start][i + 1] = VIOLET
            r[start] = tuple(r[start])
            return r
        assert can_capture(board, VIOLET, start)

        def capture(from_row, to_row):
            if board[start][from_row] == VIOLET and board[end][to_row] == CYAN:
                r[start] = list(board[start])
                r[end] = list(board[end])
                r[start][from_row] = EMPTY
                r[end][to_row] = VIOLET
                r[start] = tuple(r[start])
                r[end] = tuple(r[end])
                return True
            return False

        for row in [0, 1]:
            if capture(row, row + 1):
                return r

        raise Exception("No move is possible!")
    else:
        return exchange(move(exchange(board), VIOLET, start, end))

def is_symmetrical(board):
    """True if board is symmetric."""
    return board == flip(board)


def get_legal_moves(board, player):
    """Return a list of all legal moves for player in a given board position."""
    r = []
    for m in WELL_FORMED_MOVES:
        try:
            move(board, player, m[0], m[1])
            r.append(m)
        except:
            pass
    return r

def prune_sym_moves(moves):
    if len(moves) <= 1:
        return moves
    if flip_move(moves[0]) in moves[1:]:
        return prune_sym_moves(moves[1:])
    return prune_sym_moves(moves[1:]) + [moves[0]]

def get_forest():
    """Return a string with a LaTeX (tikz, forest) representation of the game tree and the set of unique boards.

    Boards are returned as a dictionary with the lists of turns in which each appears.
    """

    boards = {}

    def get_tree(board, player, turn, a_move, indent):
        code = to_code(board)
        if is_winner(board, other(player)):
            s = "{}[{},winner{},move={{{}}}{{{}}}]".format(indent, code,
                                                           other(player),
                                                           other(player), a_move.lower())
            return s, other(player)
        moves = get_legal_moves(board, player)
        if is_symmetrical(board):
            moves = prune_sym_moves(moves)
        if code in boards:
            for m in moves:
                boards[code][1].add(m)
        elif code[::-1] in boards:
            for m in moves:
                boards[code[::-1]][1].add(flip_move(m))
        else:
            boards[code] = (turn, set(moves))
        s = "{}[{},winner@".format(indent, code)
        if a_move != None:
            s = s + ",move={{{}}}{{{}}},winner@".format(other(player), a_move.lower())
        s = s + "\n"
        winners = []
        for m in moves:
            b = move(board, player, m[0], m[1])
            t, w = get_tree(b, other(player), turn+1, m, indent + " ")
            s = s + t + "\n"
            winners.append(w)
        if len(set(winners)) == 1:
            winner = winners[0]
        else:
            winner = player
        s = s.replace('@', str(winner))
        return s + indent + "]", winner

    f = """
%% Game tree for LEX --- Learning EX-a-pawn
%% Uncomment the lines which begin with % for a standalone LaTeX document
%\\documentclass[tikz]{standalone}
%\\usepackage{forest}
%\\begin{document}

\\forestset{
  default preamble={
    for tree={font=\\tiny}
  }
}
\\begin{forest}
  winner1/.style={draw,fill=cyan,inner sep=1pt,outer sep=0},
  winner2/.style={draw,fill=violet!60,inner sep=1pt,outer sep=0},
  move/.style n args=2{%
    if={#1<2}%
    {edge label/.expanded={%
      node [midway,fill=white,text=cyan,font=\\unexpanded{\\tiny}] {$#2$}%
    }}{edge label/.expanded={%
      node [midway,fill=white,text=violet!60,font=\\unexpanded{\\tiny}] {$#2$}%
    }},
  }
"""
    t, _ = get_tree(INITIAL_BOARD, CYAN, 1, None, " ")
    f = f + t
    f = f + "\n\\end{forest}"
    f = f + "\n%\\end{document}"

    for h in boards:
        if is_symmetrical(from_code(h)):
            moves = prune_sym_moves(list(boards[h][1]))
            boards[h] = (boards[h][0], set(moves))

    return f, boards

import unittest

class TestLex(unittest.TestCase):
    def setUp(self):
        self.board = {'X': (VIOLET, CYAN, EMPTY),
                      'Y': (EMPTY, CYAN, CYAN),
                      'Z': (VIOLET, EMPTY, EMPTY),
        }


    def test_to_code(self):
        self.assertEqual(to_code(self.board), "5C2")

    def test_flip(self):
        r = flip(self.board)
        self.assertNotEqual(r, self.board)
        self.assertEqual(r['X'], self.board['Z'])
        self.assertEqual(r['Y'], self.board['Y'])
        self.assertEqual(r['Z'], self.board['X'])

    def test_to_code_flip(self):
        r = to_code(flip(self.board))
        self.assertEqual(r, to_code(self.board)[::-1])


    def test_exchange(self):
        r = exchange(self.board)
        self.assertNotEqual(r, self.board)
        self.assertEqual(r['X'][0], EMPTY)
        self.assertEqual(r['X'][1], VIOLET)
        self.assertEqual(r['X'][2], CYAN)
        r = exchange(exchange(self.board))
        self.assertEqual(r, self.board)

    def test_can_move_fwd_violet(self):
        self.assertTrue(can_move_fwd(self.board, VIOLET, 'Z'))
        self.assertFalse(can_move_fwd(self.board, VIOLET, 'X'))

    def test_can_move_fwd_cyan(self):
        self.assertTrue(can_move_fwd(self.board, CYAN, 'Y'))
        self.assertFalse(can_move_fwd(self.board, CYAN, 'X'))

    def test_can_capture_violet(self):
        self.assertTrue(can_capture(self.board, VIOLET, 'X'))
        self.assertTrue(can_capture(self.board, VIOLET, 'Z'))
        self.assertFalse(can_capture(self.board, VIOLET, 'Y'))

    def test_can_capture_cyan(self):
        self.assertFalse(can_capture(self.board, CYAN, 'X'))
        self.assertFalse(can_capture(self.board, CYAN, 'Z'))
        self.assertTrue(can_capture(self.board, CYAN, 'Y'))


    def test_other(self):
        self.assertEqual(other(VIOLET), CYAN)
        self.assertEqual(other(CYAN), VIOLET)
        self.assertEqual(other(EMPTY), EMPTY)

    def test_winner1(self):
        w = {'X': (EMPTY, VIOLET, EMPTY),
             'Y': (EMPTY, EMPTY, VIOLET),
             'Z': (EMPTY, CYAN, EMPTY)
        }
        self.assertTrue(is_winner(w, VIOLET))
        self.assertFalse(is_winner(w, CYAN))

    def test_winner2(self):
         w = {'X': (EMPTY, VIOLET, CYAN),
              'Y': (EMPTY, EMPTY, EMPTY),
              'Z': (VIOLET, CYAN, EMPTY)
         }
         for c in ['X', 'Y', 'Z']:
             self.assertFalse(can_move_fwd(w, CYAN, c))
             self.assertFalse(can_capture(w, CYAN, c))

         self.assertTrue(is_winner(w, VIOLET))
         self.assertTrue(is_winner(w, CYAN))

    def test_move1(self):
         r = move(self.board, VIOLET, 'Z', 'Z')
         self.assertEqual(r['X'], self.board['X'])
         self.assertEqual(r['Y'], self.board['Y'])
         self.assertEqual(r['Z'], (EMPTY, VIOLET, EMPTY))

    def test_move2(self):
         r = move(self.board, VIOLET, 'X', 'Y')
         self.assertEqual(r['X'], (EMPTY, CYAN, EMPTY))
         self.assertEqual(r['Y'], (EMPTY, VIOLET, CYAN))
         self.assertEqual(r['Z'], self.board['Z'])

    def test_move3(self):
         r = move(self.board, VIOLET, 'Z', 'Y')
         self.assertEqual(r['X'], self.board['X'])
         self.assertEqual(r['Y'], (EMPTY, VIOLET, CYAN))
         self.assertEqual(r['Z'], (EMPTY, EMPTY, EMPTY))

    def test_move4(self):
        b = {'X': (EMPTY, CYAN, EMPTY),
             'Y': (VIOLET, VIOLET, EMPTY),
             'Z': (VIOLET, EMPTY, CYAN)
        }
        r = move(b, VIOLET, 'Y', 'Y')
        self.assertEqual(r['X'], b['X'])
        self.assertEqual(r['Y'], (VIOLET, EMPTY, VIOLET))
        self.assertEqual(r['Z'], b['Z'])

    def test_move5(self):
        b = {'X': (EMPTY, CYAN, EMPTY),
             'Y': (VIOLET, VIOLET, EMPTY),
             'Z': (VIOLET, EMPTY, CYAN)
        }
        r = move(b, VIOLET, 'Y', 'X')
        self.assertEqual(r['X'], (EMPTY, VIOLET, EMPTY))
        self.assertEqual(r['Y'], (EMPTY, VIOLET, EMPTY))
        self.assertEqual(r['Z'], b['Z'])

    def test_move6(self):
        b = {'X': (EMPTY, CYAN, EMPTY),
             'Y': (VIOLET, VIOLET, EMPTY),
             'Z': (VIOLET, EMPTY, CYAN)
        }
        r = move(b, VIOLET, 'Y', 'Z')
        self.assertEqual(r['X'], b['X'])
        self.assertEqual(r['Y'], (VIOLET, EMPTY, EMPTY))
        self.assertEqual(r['Z'], (VIOLET, EMPTY, VIOLET))

    def test_move7(self):
        b = {'X': (EMPTY, VIOLET, CYAN),
             'Y': (VIOLET, CYAN, EMPTY),
             'Z': (VIOLET, CYAN, EMPTY)
        }
        r = move(b, VIOLET, 'Y', 'Z')
        self.assertEqual(r['X'], b['X'])
        self.assertEqual(r['Y'], (EMPTY, CYAN, EMPTY))
        self.assertEqual(r['Z'], (VIOLET, VIOLET, EMPTY))

    def test_move8(self):
        b = {'X': (EMPTY, VIOLET, CYAN),
             'Y': (VIOLET, CYAN, EMPTY),
             'Z': (VIOLET, CYAN, EMPTY)
        }
        r = move(b, VIOLET, 'Z', 'Y')
        self.assertEqual(r['X'], b['X'])
        self.assertEqual(r['Y'], (VIOLET, VIOLET, EMPTY))
        self.assertEqual(r['Z'], (EMPTY, CYAN, EMPTY))

    def test_move9(self):
        b = INITIAL_BOARD.copy()
        r = move(b, CYAN, 'X', 'X')
        self.assertEqual(r['X'], (VIOLET, CYAN, EMPTY))
        self.assertEqual(r['Y'], b['Y'])
        self.assertEqual(r['Z'], b['Z'])

    def test_from_code(self):
        r = from_code('5C2')
        self.assertEqual(r, self.board)

    def test_from_to_code(self):
        b = INITIAL_BOARD.copy()
        r = from_code(to_code(b))
        self.assertEqual(r, b)

    def test_get_legal_moves1(self):
        r = get_legal_moves(self.board, CYAN)
        self.assertEqual(len(r), 3)
        self.assertIn('YX', r)
        self.assertIn('YY', r)
        self.assertIn('YZ', r)

    def test_get_legal_moves2(self):
        r = get_legal_moves(self.board, VIOLET)
        self.assertEqual(len(r), 3)
        self.assertIn('XY', r)
        self.assertIn('ZY', r)
        self.assertIn('ZZ', r)

    def test_is_symmetrical(self):
        self.assertTrue(is_symmetrical(from_code('B5B')))

    def test_prune_sym_moves(self):
        self.assertEqual(len(prune_sym_moves(['XX', 'ZZ'])), 1)

    def test_prune_sym_moves3(self):
        r = prune_sym_moves(['XX', 'XY', 'ZY'])
        self.assertEqual(len(r), 2)

    def test_flip_move(self):
        r = flip_move('XX')
        self.assertEqual(r, 'ZZ')
        r = flip_move('XY')
        self.assertEqual(r, 'ZY')
        r = flip_move('YZ')
        self.assertEqual(r, 'YX')
        r = flip_move('YY')
        self.assertEqual(r, 'YY')

    def test_get_forest(self):
        _, b = get_forest()
        turn2 = dict([(x, b[x]) for x in b if b[x][0] == 2])
        self.assertEqual(len(turn2), 2)
        self.assertIn('B5B', turn2)
        self.assertEqual(len(turn2['B5B']), 2)

if __name__ == '__main__':
    unittest.main()
