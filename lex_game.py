#!/usr/bin/env -S python3 -W ignore

import argparse
import random
import logging
from lex import *
from lexcel import choose_and_remember_move, reward, make_empty_experience




def get_move(board, player, turn, rand, source='input', move_memory=None):
    b = None
    while b == None:
        m = ''
        if source == 'input':
            logging.debug("From input")
            m = input('Player {} ({}), make your move: '.format(player, MARKS[player]))
        else:
            logging.debug("Choose {} {} {} {} {}".format(source, to_code(board), turn,
                                                         move_memory, player))
            logging.debug("Random {}".format(rand.getstate()))
            m = choose_and_remember_move(source, to_code(board), turn, move_memory, rand, player)
        m = m.upper()
        logging.debug("Move: {}".format(m))
        if m in WELL_FORMED_MOVES:
            try:
                b = move(board, player, m[0], m[1])
                break
            except:
                print('{} move for player {} is not allowed.'.format(m, player))
                print(to_string(board))
        else:
            print('Input is invalid.')

    print(to_string(b))
    if is_winner(b, player):
        print('Player {} won!!!'.format(player))
        return b, player
    return b, None

def learn(a_player, the_winner, mem, start, to):
    if to != None:
        if the_winner == a_player:
            # reward each move in memory
            reward(start, to, mem, 1)
        elif the_winner == other(a_player):
            # punish last move in memory
            reward(start, to, mem[-1:], -1)



def main():
    desc ="""
LEX --- Learning EX-a-pawn
A game written by Mattia Monga <mattia.monga@unimi.it> for a 'Coding for lawyers' course.
Copyright 2020 - Free to distribute, use, and modify according to the terms of GPLv3."""

    rules ="""Pawns move and capture as in chess, but there are neither two-step moves nor en-passant captures.
Players win by reaching the last row or by blocking the opponent.
Moves are given by two letters: the starting column and the ending one. Well-formed moves: {}
""".format(WELL_FORMED_MOVES)


    parser = argparse.ArgumentParser(description=desc+'\n\n'+rules,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-e1', '--exp-player1', nargs=2, metavar=('start.xlsx', 'save.xlsx'),
                        default=['input', None],
                        help='Spreadsheet files to get and save game experience used by player 1')
    parser.add_argument('-e2', '--exp-player2', nargs=2, metavar=('start.xlsx', 'save.xlsx'),
                        default=['input', None],
                        help='Spreadsheet files to get and save game experience used by player 2')


    parser.add_argument('-s', '--seed', type=int,
                        help="""Set a seed for random number generations
                        (only useful together with -e1 or -e2)""")

    parser.add_argument('-b', '--board', type=str, default=to_code(INITIAL_BOARD),
                        help='Set the initial board by giving its code (default "{}")'.format(to_code(INITIAL_BOARD)))
    parser.add_argument('-p', '--player', type=int, default=CYAN, choices=(CYAN, VIOLET),
                        help='Set the first player (default {})'.format(CYAN))
    parser.add_argument('-t', '--turn', type=int, default=1, choices=range(1, 8),
                        help='Set the turn (default {})'.format(1))

    parser.add_argument('--tree', action='store_true',
                        help='Print the game tree in LaTeX')

    parser.add_argument('--empty', type=str, metavar='empty.xlsx',
                        help='Save a spreadsheet with an empty experience')

    parser.add_argument('-d', '--debug', type=str, metavar='move.log',
                        help='Log moves in file')

    args = parser.parse_args()
    if args.debug:
        logging.basicConfig(filename=args.debug, level=logging.DEBUG)
        logging.debug("CLI Args: {}".format(args))
    if args.tree:
        print(get_forest()[0])
        exit()
    if args.empty:
        make_empty_experience(args.empty, [VIOLET, CYAN])
        exit()

    board, turn, winner = from_code(args.board), args.turn, None
    memory = {CYAN: [], VIOLET: []}
    randomizer = random.Random(args.seed)

    print(desc)
    print(to_string(board))
    print(rules)
    while winner == None:
        board, winner = get_move(board, args.player, turn, randomizer,
                                 args.exp_player1[0], memory[args.player])
        turn += 1
        if winner == None:
            board, winner = get_move(board, other(args.player), turn, randomizer,
                                     args.exp_player2[0], memory[other(args.player)])
            turn += 1

    learn(CYAN, winner, memory[CYAN], args.exp_player1[0], args.exp_player1[1])
    learn(VIOLET, winner, memory[VIOLET], args.exp_player2[0], args.exp_player2[1])

    exit()

if __name__ == '__main__':
    main()
